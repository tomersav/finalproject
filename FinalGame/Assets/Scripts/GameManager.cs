﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public Text LivesText;						// Player Lives UI
	public float Score; 						// Player Score
	public Text ScoreText;						// UI Score Text
	public float scoreFactor;					// Score Factor Increment
	public Text GameOverScoreText;				// Game over score UI Text object
	public Transform GameOverPanel;				// UI Panel Displayed when game is over
	public PlayerController ourPlayer;			// Reference to our Player Script
    public Text Victory;
    public string MainScene;

    public bool HasShownCrystalText = false;

    public MeteorSpawner MtSp;                  //Rference to the meteor spawner to control meteor spawn tempo when score is up
    public BossSpawner bsSp;

	private bool isGameStarted;                 // When isGameStarted is true Increment our player score


    public Text RedCrystal;
    public float RedCrystalVal;
    public Text PurpleCrystal;
    public float PurpleCrystalVal;
    public Text BlueCrystal;
    public float BlueCrystalVal;
    public Text WhiteCrystal;
    public float WhiteCrystalVal;
    public Image RedCrysImg;
    public Image PurpleCrysImg;
    public Image BlueCrysImg;
    public Image WhiteCrysImg;


    private int nextUpdate = 1;
    private bool HasRaised=false;
    private float LevelOfScore=100;

    public float TimeBossDestroyed = 0;
    


    public bool BossIsDead;

    // Use this for initialization
    void Start () {

		// Disable Cursor
		Cursor.visible = false;

		// When isGameStarted is true Increment our player score
		isGameStarted = true;

		// Get Our Player Script
		ourPlayer = GameObject.FindWithTag ("Player").GetComponent<PlayerController>();

		// Set Lives Text to player lives
		LivesText.text = "x " + ourPlayer.Lives;

        MtSp = GameObject.FindWithTag("MeteorSpawner").GetComponent<MeteorSpawner>(); // holding meteor spawner object

        bsSp= GameObject.FindWithTag("BossSpawner").GetComponent<BossSpawner>();

        

    }


    void Update()
    {


        if (bsSp.BossisDead)
        {
            Victory.gameObject.SetActive(true);
            
            

            
        }

        // Update Lives Text to player lives
        LivesText.text = "x " + ourPlayer.Lives;

        // if our player lives = 0 and Player exist destroy player & end game
        if (ourPlayer.Lives == 0 && ourPlayer)
        {

            // Destroy ourPlayer game object
            Destroy(ourPlayer);

            // Show game over UI panel
            ShowGameOverPanel();
        }

        // if game started increment our player score
        if (isGameStarted)
        {
            UpdateScore();


            if (Score >= 100 )
            {
                if (!HasShownCrystalText)
                {
                    RedCrystal.gameObject.SetActive(true);
                    PurpleCrystal.gameObject.SetActive(true);
                    BlueCrystal.gameObject.SetActive(true);
                    WhiteCrystal.gameObject.SetActive(true);
                    RedCrysImg.gameObject.SetActive(true);
                    PurpleCrysImg.gameObject.SetActive(true);
                    BlueCrysImg.gameObject.SetActive(true);
                    WhiteCrysImg.gameObject.SetActive(true);
                    HasShownCrystalText = true;
                }

                 RedCrystalVal=ourPlayer.RedCrystalCounter;
                 PurpleCrystalVal=ourPlayer.PurpleCrystalCounter;
                 BlueCrystalVal=ourPlayer.BlueCrystalCounter;
                 WhiteCrystalVal=ourPlayer.WhiteCrystalCounter;

                UpdateCrystalScore();
            }  


            if (Time.time >= 1 && Time.time == (int)Time.time)
            {


                if (Score > LevelOfScore && Score != 0) // Difficulty Dynamicly Building
                {
                    MtSp.MinSpeed++;
                    MtSp.MaxSpeed++;
                    if (MtSp.SpawnRate - 0.1f > 0)
                    {
                        MtSp.SpawnRate = MtSp.SpawnRate - 0.1f;
                    }
                    MtSp.SpawnStart++;
                    LevelOfScore = LevelOfScore + 100;


                    if (Score > 300) // level of difficulty
                    {
                        MtSp.BigMaxSpeed++;
                        MtSp.BigMinSpeed++;
                        if (MtSp.BigSpawnRate - 0.6f > 0)
                            MtSp.BigSpawnRate = MtSp.BigSpawnRate - 0.6f;
                        MtSp.BigSpawnStart++;


                        MtSp.CollectableMaxSpeed++;
                        MtSp.CollectableMinSpeed++;
                        if (MtSp.CollectableSpawnRate - 0.6f > 0)
                            MtSp.CollectableSpawnRate = MtSp.CollectableSpawnRate - 0.6f;
                        MtSp.CollectableSpawnStart++;

                    }

                }
            }

        }
    }

    // Show GameOverPanel , Stop Time and Set Cursor to visible 
    void ShowGameOverPanel()
	{
		Time.timeScale = 0;
		isGameStarted = false;
		Cursor.visible = true;
		GameOverScoreText.text += Score.ToString ("F0");
		GameOverPanel.gameObject.SetActive (enabled);
	}
    
    public void StopALLGraphic()
    {
        
        RedCrystal.gameObject.SetActive(false);
        PurpleCrystal.gameObject.SetActive(false);
        BlueCrystal.gameObject.SetActive(false);
        WhiteCrystal.gameObject.SetActive(false);
        RedCrysImg.gameObject.SetActive(false);
        PurpleCrysImg.gameObject.SetActive(false);
        BlueCrysImg.gameObject.SetActive(false);
        WhiteCrysImg.gameObject.SetActive(false);
        ScoreText.gameObject.SetActive(false);
    }
	// Update the scoreText element every delta time multiply scoreFactor
	void UpdateScore()
	{
        
		ScoreText.text = "Score : " + Score.ToString("F0");
	}
    void UpdateCrystalScore()
    {
        RedCrystal.text = RedCrystalVal.ToString("F0");
        PurpleCrystal.text = PurpleCrystalVal.ToString("F0");
        BlueCrystal.text = BlueCrystalVal.ToString("F0");
        WhiteCrystal.text = WhiteCrystalVal.ToString("F0");

        
    }

    // Reload current scene and reset time Scale to 1
    public void PlayAgain()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene (MainScene);
	}

    
}
