using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;


public class BossController : MonoBehaviour
{


    
    float BossOffset;
    public float BossSpeed=0;                       // Meteor Speed
    public Vector2 Direction;
    public int BossHits = 0;                       //need 3 hits to be destroyed
    float maxXright;
    float maxXleft;
    public GameManager Gm;
    Rigidbody2D rb2d;
    public GameObject MeteorExplostionPrefab;
    public bool JustCreated = true;
    public BossSpawner bsSp;
    public FireBallSpawner FBsp;


    public bool isBossDestroyed=false;
    

    // Use this for initialization
    void Start()
    {

        
        rb2d = GetComponent<Rigidbody2D>();
       
        Gm = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
        
        StartCoroutine(DelayBoss(3));
        BossSpeed = 3;
        int RandomIndex = Random.Range(0, 1);
        if (RandomIndex == 0)
        {
            RandomIndex = -1;
        }
        Direction = new Vector2(RandomIndex, 0);

        bsSp = GameObject.FindWithTag("BossSpawner").GetComponent<BossSpawner>();
        FBsp = GameObject.FindWithTag("FireBallSpawner").GetComponent<FireBallSpawner>();

    }

     IEnumerator DelayBoss(int num)
    {
        yield return new WaitForSeconds(num);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
          
            rb2d.velocity = Direction * BossSpeed;
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("WallRight"))
        {
            Direction = new Vector2(-1, 0);
            rb2d.velocity = Direction * BossSpeed;

        }
        if (other.CompareTag("WallLeft"))
        {

            Direction = new Vector2(1, 0);
            rb2d.velocity = Direction * BossSpeed;

        }
        if (other.CompareTag("Laser"))
        {
            BossHits++;
            if (BossHits == 100)
            {
                Instantiate(MeteorExplostionPrefab, transform.position, Quaternion.identity);
                
                bsSp.BossisDead = true;
                bsSp.CancelInvoke();
                FBsp.CancelInvoke();
                Gm.TimeBossDestroyed = Time.timeScale;
                Destroy(this.gameObject);
                
              
            }

        }
        if (other.CompareTag("WeakLaser"))
        {
            BossHits++;
            if (BossHits == 200)
            {
                Instantiate(MeteorExplostionPrefab, transform.position, Quaternion.identity);

                bsSp.BossisDead = true;
                bsSp.CancelInvoke();
                FBsp.CancelInvoke();
                Destroy(this.gameObject);


            }

        }
    }
    }
