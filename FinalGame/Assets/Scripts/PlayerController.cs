﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public int Lives;								// Player lives count
	public float numBlinks;							// Number of blinks to do when respawning
	public float blinkSeconds;						// Time between blinks
    public bool canShoot = true;

    public int RedCrystalCounter=0;
    public int PurpleCrystalCounter=0;
    public int BlueCrystalCounter=0;
    public int WhiteCrystalCounter=0;

    public Transform laserSpwan;					// Laser spawn position
	public GameObject laserPrefab;					// Laser prefab game object
    public GameObject WeakLaserPrefarb;
    public bool WeakLaserTime = false;
    public float WeakLaserTimeCreation = 0;




    private float maxWidth;							// Defines the X axis range we can't cross outside our screen	
	private float maxHeight;						// Defines the Y axis range we can't cross outside our screen

	private AudioSource audioSource;				// Reference to Audio Source component so we can play sounds

    public BoxCollider2D boxCollider2D;

    // Use this for initialization
    void Start () {


        boxCollider2D = GetComponent<BoxCollider2D>();

        // Get reference to this game object Audio Source component
        audioSource = GetComponent<AudioSource> ();

		// Find screen upper corner and convert it to world point
		Vector3 upperCorner = new Vector3 (Screen.width, Screen.height, 0);
		Vector3 targetWidth = Camera.main.ScreenToWorldPoint (upperCorner);

		// Get player extents in the y+x axis 
		float playerWidth = GetComponent<Renderer> ().bounds.extents.x;
		float playerHeight = GetComponent<Renderer> ().bounds.extents.y;

		// Decrease our player extents from the moveable boundaries so we be seen half way offscreen  
		maxWidth = targetWidth.x - playerWidth;
		maxHeight = targetWidth.y - playerHeight;

	}
	
	// Update is called once per frame
	void Update() {

		// if we pressed left mouse button call fire function
		if (Input.GetMouseButtonDown(0) && canShoot) {
            canShoot = false;
            Fire();
            StartCoroutine(ShootDelay());
            
		}
        if (WeakLaserTime)
        {
            float timeSinceInitialization = Time.timeSinceLevelLoad - WeakLaserTimeCreation;
            if (timeSinceInitialization >= 7f)
            {

                WeakLaserTime = false;

            }
        }

		UpdatePosition ();
		
	}

    // Instantiate a laser prefabe and playing laser sound
    void Fire()
	{
        if (!WeakLaserTime)
        {

            Instantiate(laserPrefab, laserSpwan.position, Quaternion.identity);
            audioSource.PlayOneShot(audioSource.clip);
        }
        else
        {
            Instantiate(WeakLaserPrefarb, laserSpwan.position, Quaternion.identity);
            audioSource.PlayOneShot(audioSource.clip);
        }
        
    }


	// Update player position based on the mouse position
	void UpdatePosition()
	{

		Vector3 rawPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		Vector3 targetPosition = new Vector3 (rawPosition.x, rawPosition.y, 0.0f);

		float targetWidth = Mathf.Clamp (targetPosition.x, -maxWidth, maxWidth);
		float targetHeight = Mathf.Clamp (targetPosition.y, -maxHeight, maxHeight);

		targetPosition = new Vector3 (targetWidth, targetHeight, targetPosition.z);

		transform.position = targetPosition;

	}

	// Check for collisions
	void OnTriggerEnter2D(Collider2D other)
	{
		// if we collide with Meteor destroy player
		if (other.CompareTag("Meteor")) {
			DestroyPlayer ();
		}
        // if we collide with Meteor destroy player
        if (other.CompareTag("BigMeteor"))
        {
            DestroyPlayer();
        }
        if (other.CompareTag("BossLaser"))
        {
            DestroyPlayer();
        }
        if (other.CompareTag("Boss"))
        {
            DestroyPlayer();
        }
        if (other.CompareTag("FireBall"))
        {
            DestroyPlayer();
        }

    }

	// Decrease 1 from our player lives and start Respawning 
	void DestroyPlayer()
	{
		Lives--;
		StartCoroutine(RespawnPlayer(numBlinks,blinkSeconds));
	}

	// Blinks our player upon respawning and make him invulnerable while respawning
	IEnumerator RespawnPlayer(float numBlinks,float seconds )
	{
		Renderer renderer = GetComponent<Renderer> ();
		boxCollider2D = GetComponent<BoxCollider2D> ();

		// Disable Our Box Collider 2D so we can't be hit while respawning
		boxCollider2D.enabled = false;

		// Make our player blink
		for (int i = 0; i < numBlinks*2; i++) {

			// Toggle renderer
			renderer.enabled = !renderer.enabled;

			// Wait for a x Seconds
			yield return new WaitForSeconds (seconds);

		}

		// Enable our box coliider 2D so we can get hit
		boxCollider2D.enabled = true;

		// Make sure renderer is enabled when we exit
		renderer.enabled = true;

	}



    IEnumerator ShootDelay()
    {
        yield return new WaitForSeconds(0.2f);
        canShoot = true;
    }
}

