using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallSpawner : MonoBehaviour
{
    public GameObject FireBallPrefab;


    public float SpawnRate;                     // Meteors Spwan rate
    public float SpawnStart;					// Meteors Spwan Start Time

    Vector2 min;                                // Viewport Bottom Left Point
    Vector2 max;                                // Viewport Top Right Point

    public BossSpawner bsSp;
    bool hasInvoked = false;

    // Start is called before the first frame update
    void Start()
    {
        // Viewport bottom Left Point
        min = Camera.main.ViewportToWorldPoint(new Vector3(0, 0));

        // Viewport Top right point
        max = Camera.main.ViewportToWorldPoint(new Vector3(1, 1));



        bsSp = GameObject.FindWithTag("BossSpawner").GetComponent<BossSpawner>();





    }

    // Update is called once per frame
    void Update()
    {
        if (bsSp.BossTime && !hasInvoked) // we want to limit the repeat invoke to one time during running to check score
                                            // if we wont limit the invoke it will be called every frame 
        {
            hasInvoked = true;
            InvokeRepeating("SpawnFireBall", SpawnStart, SpawnRate);
            


        }
    }



    void SpawnFireBall()
    {

        // Get Meteor Sprite Y+X extents
        float FireBallExtentsX = FireBallPrefab.GetComponent<Renderer>().bounds.extents.x;
        float FireBallExtentsY = FireBallPrefab.GetComponent<Renderer>().bounds.extents.y;

        // Generate random position on the X axis
        float randomX = Random.Range(min.x + FireBallExtentsX, max.x - FireBallExtentsX);

        // Set Our Random Position + Y offset so the meteor will spawn outside our view
        Vector2 randomPosition = new Vector2(randomX, max.y + FireBallExtentsY);

        // Instantiate our meteor
        GameObject FireBall = Instantiate(FireBallPrefab, randomPosition, Quaternion.identity);

        // Set Meteor's Random Speed
        FireBall.GetComponent<FireBallController>().FireBallSpeed = 15f;

        

    }
}
