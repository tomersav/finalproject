using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpItemController : MonoBehaviour
{
    public Sprite[] Sprites;
    public float PickUpSpeed;
    float maxY;
    public GameManager Gm;
    public PlayerController ourPlayer;
    Rigidbody2D rb2d;
    BoxCollider2D boxcollider2D;
    bool ColliderOfPlayer;

    private float initializationTime;

    public GameObject ShieldPrefarb;
    public GameObject FreezePrefarb;
    

    
    


    // Start is called before the first frame update
    void Start()
    {

        rb2d = GetComponent<Rigidbody2D>();
        boxcollider2D = GetComponent<BoxCollider2D>();
        int RandomIndex = UnityEngine.Random.Range(0, Sprites.Length);
        GetComponent<SpriteRenderer>().sprite = Sprites[RandomIndex];
        maxY = Camera.main.ViewportToWorldPoint(new Vector3(0, 0)).y;
        Gm = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
        ourPlayer = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
        StartCoroutine(SpawnPickUpItem());
        



    }

    // Update is called once per frame
    void Update()
    {
        // if we passed maxY (=outside screen) position destroy gameobject
        if (transform.position.y < maxY)
        {
            Destroy(this.gameObject);
            Gm.Score = Gm.Score - 15; // lower score when missing to collect item
            
            
        }

        transform.Translate(Vector3.down * PickUpSpeed * Time.deltaTime); // Update our Position

        
    }


    IEnumerator SpawnPickUpItem()
    {

        boxcollider2D.isTrigger = false;
        yield return new WaitForSeconds(0.3f);
        boxcollider2D.isTrigger = true;
    }

    // Check for collisions
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {

            if (GetComponent<SpriteRenderer>().sprite == Sprites[0]) //Red Heart
            {

                ourPlayer.Lives--;
                

            }
            if (GetComponent<SpriteRenderer>().sprite == Sprites[1]) //Shield
            {

                Instantiate(ShieldPrefarb, ourPlayer.transform.position, Quaternion.identity);
                
             
                


            }
            if (GetComponent<SpriteRenderer>().sprite == Sprites[2]) //Tresure
            {
                Gm.Score = Gm.Score + 20;
            }
            if (GetComponent<SpriteRenderer>().sprite == Sprites[3]) //Yellow Heart
            {
                ourPlayer.Lives++;
            }
            if (GetComponent<SpriteRenderer>().sprite == Sprites[4]) //Freeze
            {

                Instantiate(FreezePrefarb, ourPlayer.transform.position, Quaternion.identity);
            }
            if (GetComponent<SpriteRenderer>().sprite == Sprites[5]) //Gear.Weak Laser
            {

                ourPlayer.WeakLaserTime = true;
                ourPlayer.WeakLaserTimeCreation= Time.timeSinceLevelLoad;
            }

            Destroy(this.gameObject);


        }

    }

     
}
