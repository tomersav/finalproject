using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableController : MonoBehaviour
{

    public Sprite[] Sprites;
    public float CollectableSpeed;
    float maxY;
    public GameManager Gm;
    public PlayerController ourPlayer;


    // Start is called before the first frame update
    void Start()
    {
        int RandomIndex = Random.Range(0, Sprites.Length);
        GetComponent<SpriteRenderer>().sprite = Sprites[RandomIndex];
        maxY = Camera.main.ViewportToWorldPoint(new Vector3(0, 0)).y;
        Gm = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
        ourPlayer = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        // if we passed maxY (=outside screen) position destroy gameobject
        if (transform.position.y < maxY)
        {
            Destroy(this.gameObject);
            Gm.Score = Gm.Score - 15; // lower score when missing to collect item
        }

        // Update our Position
        transform.Translate(Vector3.down * CollectableSpeed * Time.deltaTime);
    }

    // Check for collisions
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {

            Destroy(this.gameObject);

            if (GetComponent<SpriteRenderer>().sprite == Sprites[0]) // red
            {
                ourPlayer.RedCrystalCounter++;
            }
            else if (GetComponent<SpriteRenderer>().sprite == Sprites[1]) //Purple
            {
                ourPlayer.PurpleCrystalCounter++;
            }
            else if (GetComponent<SpriteRenderer>().sprite == Sprites[2]) //Blue
            {
                ourPlayer.BlueCrystalCounter++;
            }
            else if (GetComponent<SpriteRenderer>().sprite == Sprites[3]) //White
            {
                ourPlayer.WhiteCrystalCounter++;
            }




        }



    

    }

   


    }
