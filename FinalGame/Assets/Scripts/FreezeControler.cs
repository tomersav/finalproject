using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeControler : MonoBehaviour
{
    // Start is called before the first frame update
    

        public PlayerController ourPlayer;
        private float initializationTime;
        // Start is called before the first frame update
        void Start()
        {
            ourPlayer = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
            initializationTime = Time.timeSinceLevelLoad;
            ourPlayer.canShoot = false;

    }

        // Update is called once per frame
        void Update()
        {
            transform.position = ourPlayer.transform.position;
            float timeSinceInitialization = Time.timeSinceLevelLoad - initializationTime;
            if (timeSinceInitialization >= 8f)
            {
                Destroy(this.gameObject);
            ourPlayer.canShoot = true;

            }

        }

}
