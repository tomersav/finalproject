

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigMeteorController : MonoBehaviour
{

    public GameObject PickUpItemPrefarb;       

    public GameObject MeteorExplostionPrefab;       // Meteor explostion animation

    public float meteorSpeed;                       // Meteor Speed

    public int meteorHits = 0;                       //need 3 hits to be destroyed

    float maxY;                                     // Bottom most Y axis screen position , if we pass this position destroy game object

    public GameManager Gm;

    public bool isMeteorDestroyed;


    // Use this for initialization
    void Start()
    {
        isMeteorDestroyed = false;
        // Viewport Bottom Left Point
        maxY = Camera.main.ViewportToWorldPoint(new Vector3(0, 0)).y;
        Gm = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();

    }

    // Update is called once per frame
    void Update()
    {

        // if we passed maxY (=outside screen) position destroy gameobject
        if (transform.position.y < maxY)
        {
            Destroy(this.gameObject);
            Gm.Score = Gm.Score - 15;

        }

        // Update our Meteor Position
        transform.Translate(Vector3.down * meteorSpeed * Time.deltaTime);

    }

    // Check for collisions
    void OnTriggerEnter2D(Collider2D other)
    {
        // if other collider is of type laser\player instantiate a meteor Explostion and destroy this gameobject
        if (other.CompareTag("Laser"))
        {
            meteorHits++;

            if (meteorHits == 3)
            {
                
                Instantiate(MeteorExplostionPrefab, transform.position, Quaternion.identity);
                Gm.Score = Gm.Score + 15;
                int random = Random.Range(0, 2);
                if (random < 1)
                {
                    Instantiate(PickUpItemPrefarb, transform.position, Quaternion.identity);
                }
                



                Destroy(this.gameObject);
            }
        }
        if (other.CompareTag("WeakLaser"))
        {
            meteorHits++;

            if (meteorHits == 6)
            {

                Instantiate(MeteorExplostionPrefab, transform.position, Quaternion.identity);
                Gm.Score = Gm.Score + 15;
                int random = Random.Range(0, 2);
                if (random < 1)
                {
                    Instantiate(PickUpItemPrefarb, transform.position, Quaternion.identity);
                }




                Destroy(this.gameObject);
            }
        }

    }

}
