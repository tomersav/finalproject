using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BossSpawner : MonoBehaviour
{
    public GameObject BossPrefarb;
    Vector2 min;                                // Viewport Bottom Left Point
    Vector2 max;                                // Viewport Top Right Point
    public bool BossTime = false;
    bool EnoughCrystals = false;
    public GameManager Gm;
    public MeteorSpawner MeteorSpawner;

    public GameObject bckgr;
    public Sprite NewBackground;

    public Transform laserSpwanLeft;
    public Transform laserSpwanRight;
    public GameObject laserPrefab;
    public GameObject LeftLaser;
    public GameObject RightLaser;
    public float LaserBossRate;                     
    public float LaserBossStart;

    public bool BossisDead=false;

    public Text BossText;
    public PlayerController ourPlayer;
    public MeteorSpawner Spawner;
    public bool JustCreated = true;
    public bool canFire = false;



    // Start is called before the first frame update
    void Start()
    {
        // Viewport bottom Left Point
        min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

        // Viewport Top right point
        max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));


        Gm = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
        ourPlayer = GameObject.FindWithTag("Player").GetComponent<PlayerController>();

        Spawner = GameObject.FindWithTag("MeteorSpawner").GetComponent<MeteorSpawner>();



        







    }

    void FixedUpdate()
    {
        if (ourPlayer.RedCrystalCounter > 5 && ourPlayer.PurpleCrystalCounter > 5 && ourPlayer.BlueCrystalCounter > 5
            && ourPlayer.WhiteCrystalCounter > 5) { 
            EnoughCrystals = true; }

        if (Gm.Score >= 500 && !BossTime && EnoughCrystals) // we want to limit the repeat invoke to one time during running to check score
                                                           // if we wont limit the invoke it will be called every frame 
        {

            
            bckgr.GetComponent<SpriteRenderer>().sprite = NewBackground;
            bckgr.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = NewBackground;
            Spawner.StopAllCoroutines();
            Spawner.BossTime = true;

            
            if (JustCreated)
            {
                StartCoroutine(BossMessage());
            }
            
            GameObject Boss = Instantiate(BossPrefarb, transform.position, Quaternion.identity);

            if (!BossisDead)
                {
                
                    laserSpwanRight = Boss.transform.GetChild(0);
                    laserSpwanLeft = Boss.transform.GetChild(1);
                
                if (!canFire)
                {
                    StartCoroutine(Delay());
                }
                
                
                    InvokeRepeating("Fire", LaserBossStart, LaserBossRate);
                
                    BossTime = true;
                 }
                Gm.StopALLGraphic();


            
            
        }
    }
    

    

    IEnumerator BossMessage()
    {
        BossText.gameObject.SetActive(true);
        yield return new WaitForSeconds(2f);
        BossText.gameObject.SetActive(false);
        JustCreated = false;
        


    }
    IEnumerator Delay()
    {
        
        yield return new WaitForSeconds(4f);
        canFire = true;
        



    }

    public void Fire()
        {

        if (!BossisDead )
        {
            LeftLaser = Instantiate(laserPrefab, laserSpwanLeft.position, Quaternion.identity);
            LeftLaser.transform.Rotate(new Vector3(0, 0, 270));
            RightLaser = Instantiate(laserPrefab, laserSpwanRight.position, Quaternion.identity);
            RightLaser.transform.Rotate(new Vector3(0, 0, 270));
        }

    }



    
}
