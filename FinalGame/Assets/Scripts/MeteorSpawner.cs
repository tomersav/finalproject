﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorSpawner : MonoBehaviour {

	public GameObject MeteorPrefab;
    public GameObject BigMeteorPrefab;
    public GameObject CollectablePrefarb;
    

    public float MinSpeed;						// Min Meteor Speed
	public float MaxSpeed;						// Max Meteor Speed
	public float SpawnRate;						// Meteors Spwan rate
	public float SpawnStart;					// Meteors Spwan Start Time

	Vector2 min;								// Viewport Bottom Left Point
	Vector2 max;                                // Viewport Top Right Point
    bool hasInvoked = false;
    public bool BossTime = false;
    

    public GameManager Gm;
    public float BigMinSpeed;                      // Min  Big Meteor Speed
    public float BigMaxSpeed;                      // Max Big Meteor Speed
    public float BigSpawnRate;                     // Big Meteors Spwan rate
    public float BigSpawnStart;                 // Big Meteors Spwan Start Time

    public float CollectableMinSpeed;                      // Min  Big Meteor Speed
    public float CollectableMaxSpeed;                      // Max Big Meteor Speed
    public float CollectableSpawnRate;                     // Big Meteors Spwan rate
    public float CollectableSpawnStart;					// Big Meteors Spwan Start Time






     



    // Use this for initialization
    void Start () {

		// Viewport bottom Left Point
		min = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0));

		// Viewport Top right point
		max = Camera.main.ViewportToWorldPoint(new Vector3(1,1));

		// repeat SpawnMeteor function every SpawnRate and start at SpawnStart time
		InvokeRepeating ("SpawnMeteor", SpawnStart, SpawnRate);

        Gm = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();



       

    }


    void FixedUpdate() {


        if (Gm.Score == 100 && !hasInvoked) // we want to limit the repeat invoke to one time during running to check score
            // if we wont limit the invoke it will be called every frame 
        {
            hasInvoked = true;
            InvokeRepeating("SpawnBigMeteor", BigSpawnStart, BigSpawnRate);
            InvokeRepeating("SpawnCollectable", CollectableSpawnStart, CollectableSpawnRate);


        }


    }

    

    void SpawnMeteor()
	{
		
		// Get Meteor Sprite Y+X extents
		float meteorExtentsX = MeteorPrefab.GetComponent<Renderer> ().bounds.extents.x;
		float meteorExtentsY = MeteorPrefab.GetComponent<Renderer> ().bounds.extents.y;

		// Generate random position on the X axis
		float randomX = Random.Range (min.x + meteorExtentsX, max.x - meteorExtentsX);

		// Set Our Random Position + Y offset so the meteor will spawn outside our view
		Vector2 randomPosition = new Vector2 (randomX, max.y + meteorExtentsY);

		// Instantiate our meteor
		GameObject Meteor = Instantiate (MeteorPrefab, randomPosition, Quaternion.identity);

		// Set Meteor's Random Speed
		Meteor.GetComponent<MeteorController> ().meteorSpeed = Random.Range (MinSpeed, MaxSpeed);

        if (BossTime)
        {
            CancelInvoke("SpawnMeteor");
            CancelInvoke("SpawnBigMeteor");
            CancelInvoke("SpawnCollectable");
        }

    }





    void SpawnBigMeteor()
    {

        // Get Meteor Sprite Y+X extents
        float meteorExtentsX = BigMeteorPrefab.GetComponent<Renderer>().bounds.extents.x;
        float meteorExtentsY = BigMeteorPrefab.GetComponent<Renderer>().bounds.extents.y;

        // Generate random position on the X axis
        float randomX = Random.Range(min.x + meteorExtentsX, max.x - meteorExtentsX);

        // Set Our Random Position + Y offset so the meteor will spawn outside our view
        Vector2 randomPosition = new Vector2(randomX, max.y + meteorExtentsY);

        // Instantiate our meteor
        GameObject Meteor = Instantiate(BigMeteorPrefab, randomPosition, Quaternion.identity);

        // Set Meteor's Random Speed
        Meteor.GetComponent<BigMeteorController>().meteorSpeed = Random.Range(BigMinSpeed, BigMaxSpeed);

        if (BossTime)
        {
            CancelInvoke("SpawnBigMeteor");
        }

    }


    void SpawnCollectable()
    {

        // Get Meteor Sprite Y+X extents
        float meteorExtentsX = CollectablePrefarb.GetComponent<Renderer>().bounds.extents.x;
        float meteorExtentsY = CollectablePrefarb.GetComponent<Renderer>().bounds.extents.y;

        // Generate random position on the X axis
        float randomX = Random.Range(min.x + meteorExtentsX, max.x - meteorExtentsX);

        // Set Our Random Position + Y offset so the meteor will spawn outside our view
        Vector2 randomPosition = new Vector2(randomX, max.y + meteorExtentsY);

        // Instantiate our meteor
        GameObject Collectable = Instantiate(CollectablePrefarb, randomPosition, Quaternion.identity);

        // Set Meteor's Random Speed
        Collectable.GetComponent<CollectableController>().CollectableSpeed = Random.Range(CollectableMinSpeed, CollectableMaxSpeed);

    }






}
