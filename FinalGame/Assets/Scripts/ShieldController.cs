using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldController : MonoBehaviour
{
    public PlayerController ourPlayer;
    private float initializationTime;
    // Start is called before the first frame update
    void Start()
    {
        ourPlayer = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
        initializationTime = Time.timeSinceLevelLoad;
        GetComponent<CircleCollider2D>().isTrigger = false;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = ourPlayer.transform.position;
        float timeSinceInitialization = Time.timeSinceLevelLoad - initializationTime;
        if (timeSinceInitialization >= 5f)
        {
            Destroy(this.gameObject);
            ourPlayer.GetComponent<BoxCollider2D>().isTrigger = true;
        }

    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("BigMeteor"))
        {
            collision.gameObject.SetActive(false);
        }
        if (collision.CompareTag("Meteor"))
        {
            collision.gameObject.SetActive(false);
        }
        if (collision.CompareTag("PickUpItem"))
        {
            collision.gameObject.SetActive(false);
        }
        if (collision.CompareTag("Collectable"))
        {
            collision.gameObject.SetActive(false);
        }
        if (collision.CompareTag("Laser"))
        {
            collision.gameObject.SetActive(false);
        }
        if (collision.CompareTag("BossLaser"))
        {
            collision.gameObject.SetActive(false);
        }
    }
}
