using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManagerScript : MonoBehaviour
{
    public string FirstScene;
    public Transform InsPanel;
    // Start is called before the first frame update
    public void play()
    {
        SceneManager.LoadScene(FirstScene);
    }

    // Update is called once per frame
    public void Exit()
    {
        Application.Quit();
    }

    public void Ins()
    {
        InsPanel.gameObject.SetActive(enabled);
    }

    public void ExitPanel()
    {
        InsPanel.gameObject.SetActive(!enabled);
    }
}
