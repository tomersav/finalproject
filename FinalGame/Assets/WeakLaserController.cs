

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeakLaserController : MonoBehaviour
{

    public float Speed;                             // Laser Speed
    float offscreenY;								// Screen Top Y offset
    public GameManager Gm;

    // Use this for initialization
    void Start()
    {

        // Get Viewport Top Right Point in the Y axis
        offscreenY = Camera.main.ViewportToWorldPoint(new Vector2(1, 1)).y;
        Gm = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {

        // Check if our laser moved offscreen in the Y axis then destroy it else keep moving up
        if (transform.position.y > offscreenY)
        {
            Destroy(this.gameObject);
        }
        else
        {
            transform.Translate(Vector3.up * Speed * Time.deltaTime);
        }
    }

    
    

}
